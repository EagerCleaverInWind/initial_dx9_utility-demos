#include "D3DApp.h"

LRESULT  CALLBACK  DefaultWndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	//e_pCamera->HandleMessages(hwnd,message,wParam,lParam);//如果不if，连窗口都不会出现，因为消息处理函数比Object_Init()先被调用
	if(e_pCamera)
		e_pCamera->HandleMessages(hwnd,message,wParam,lParam);

	switch (message)
	{
	case   WM_KEYDOWN:
		if(wParam==VK_ESCAPE)
			DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hwnd,message,wParam,lParam);
	}

	return 0;
}

bool D3DApp::Window_Init(HINSTANCE hInstance,int nShowCmd,WNDPROC wndProc)
{
	//mWndClass={0};这样清零不行，要在结构体初始化时才能这样
	WNDCLASSEX wndClass={0};
	mWndClass=wndClass;
	mWndClass.cbSize=sizeof(WNDCLASSEX);
	mWndClass.style=CS_HREDRAW|CS_VREDRAW;
	if(wndProc)
	{
		mWndClass.lpfnWndProc=wndProc;
	}
	else
	{
		mWndClass.lpfnWndProc=DefaultWndProc;
	}
	mWndClass.cbClsExtra=0;
	mWndClass.cbWndExtra=0;
	mWndClass.hInstance=hInstance;
	mWndClass.hIcon=(HICON)::LoadImage(NULL,_T("icon.ico"),IMAGE_ICON,0,0,LR_DEFAULTSIZE|LR_LOADFROMFILE);
	mWndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
	mWndClass.hbrBackground=(HBRUSH)GetStockObject(GRAY_BRUSH);
	mWndClass.lpszMenuName=NULL;
	mWndClass.lpszClassName=mWndClassName;

	if(!RegisterClassEx(&mWndClass))
		return false;

	mhwnd=CreateWindow(mWndClassName,mWndTittleName,WS_OVERLAPPEDWINDOW,0,0,
		WINDOW_WIDTH,WINDOW_HEIGHT,NULL,NULL,hInstance,NULL);
	MoveWindow(mhwnd,250,80,WINDOW_WIDTH,WINDOW_HEIGHT,true);
	ShowWindow(mhwnd,nShowCmd);
	UpdateWindow(mhwnd);
	return true;
}

HRESULT  D3DApp::Direct3D_Init(HWND hwnd)
{
	//创建D3D接口
	LPDIRECT3D9		PD3D=NULL;
	if(NULL==(PD3D=Direct3DCreate9(D3D_SDK_VERSION)))
		return E_FAIL;

	//获取硬件设备信息
	D3DCAPS9 caps;
	int vp=0;
	if(FAILED(PD3D->GetDeviceCaps(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,&caps) ) )
		return E_FAIL;
	if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		vp=D3DCREATE_HARDWARE_VERTEXPROCESSING;
	else
		vp=D3DCREATE_SOFTWARE_VERTEXPROCESSING;

	// Check for vertex shader version 2.0 support.
	if( caps.VertexShaderVersion < D3DVS_VERSION(2, 0) )
		return false;

	// Check for pixel shader version 2.0 support.
	if( caps.PixelShaderVersion < D3DPS_VERSION(2, 0) )
		return false;


	//填充D3DPRESENT_PARAMETERS结构体
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp,sizeof(d3dpp));
	d3dpp.BackBufferWidth=WINDOW_WIDTH;
	d3dpp.BackBufferHeight=WINDOW_HEIGHT;
	d3dpp.BackBufferFormat=D3DFMT_A8R8G8B8;
	d3dpp.BackBufferCount=1;
	d3dpp.MultiSampleType=D3DMULTISAMPLE_NONE;
	d3dpp.MultiSampleQuality=0;
	d3dpp.SwapEffect=D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow=hwnd;
	d3dpp.Windowed=true;
	d3dpp.EnableAutoDepthStencil=true;
	d3dpp.AutoDepthStencilFormat=D3DFMT_D24S8;
	d3dpp.Flags=0;
	d3dpp.FullScreen_RefreshRateInHz=0;
	d3dpp.PresentationInterval=D3DPRESENT_INTERVAL_IMMEDIATE;

	//创建D3D设备
	if(FAILED(PD3D->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,hwnd,vp,&d3dpp,&e_pd3dDevice) ) )
		return E_FAIL;

	SAFE_RELEASE(PD3D);
	return S_OK;
}

HRESULT	D3DApp::Direct3D_Render(float timeDelta)
{
	e_pd3dDevice->Clear(0,NULL,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,BLACK,1.0f,0);//最开始没有添加D3DCLEAR_ZBUFFER的清理，静态还好，动态的时候老是出现逐渐被黑色吞没的问题……快哭了
	e_pd3dDevice->BeginScene();
	Draw();
	ShowFPS(mpFont,D3DCOLOR_XRGB(180,96,96),WINDOW_WIDTH,WINDOW_HEIGHT,timeDelta,true);
	e_pd3dDevice->EndScene();
	e_pd3dDevice->Present(NULL,NULL,NULL,NULL);

	ScreenShot();
	return S_OK;
}


LRESULT  D3DApp::Object_Init(HINSTANCE hInstance,HWND hwnd)
{
	srand((unsigned int)time(0));
	D3DXCreateFont(e_pd3dDevice, 28, 0, 1000, 0, false, DEFAULT_CHARSET, 
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, 0, _T("微软雅黑"), &mpFont); 

	e_hMainWnd=hwnd;
	e_pDInput=new DirectInput(hInstance,hwnd);
	e_pArcball=new ArcBall(WINDOW_WIDTH,WINDOW_HEIGHT);
	e_pCamera=new FPAirCamera();
	D3DXCreateTextureFromFile(e_pd3dDevice,"Texture/whitetex.dds",&e_pWhiteTexture);

	return S_OK;
}

void D3DApp::Update(float timeDelta)
{
	//注意顺序
	e_pDInput->Poll();
	e_pCamera->Update(timeDelta);
}

void D3DApp::Draw()
{

}

D3DApp::D3DApp(LPCSTR wndClassName,LPCSTR wndTittleName,HINSTANCE hInstance,int nShowCmd,WNDPROC wndProc)
	:mWndClassName(wndClassName),mWndTittleName(wndTittleName)
{
	Window_Init(hInstance,nShowCmd,wndProc);
	Direct3D_Init(mhwnd);
	//Object_Init(hInstance,mhwnd);//坑！在父类构造函数中没法多态！因为此时子类还没构造出来！
}

D3DApp::~D3DApp()
{
	SAFE_RELEASE(mpFont);

	SAFE_RELEASE(e_pWhiteTexture);
	SAFE_DELETE(e_pCamera);
	SAFE_DELETE(e_pArcball);
	SAFE_DELETE(e_pDInput)
	SAFE_RELEASE(e_pd3dDevice);

	UnregisterClass(mWndClassName,mWndClass.hInstance);
}

void D3DApp::Run()
{
	Object_Init(mWndClass.hInstance,mhwnd);

	MSG msg={0};
	__int64 cntsPerSec=0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&cntsPerSec);
	float secsPerCnt=1.0f/(float)cntsPerSec;
	__int64 prevCnts,currentCnts;
	float dt;
	QueryPerformanceCounter((LARGE_INTEGER*)&prevCnts);
	while (msg.message!=WM_QUIT)
	{
		if (PeekMessage(&msg,0,0,0,PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			QueryPerformanceCounter((LARGE_INTEGER*)&currentCnts);
			dt=(currentCnts-prevCnts)*secsPerCnt;
			Update(dt);
			Direct3D_Render(dt);
			prevCnts=currentCnts;
		}
	}

}