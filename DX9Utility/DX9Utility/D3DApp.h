#pragma  once

#include <time.h>
#include "Utility.h"
#include "DirectInput.h"
#include "Camera.h"
#include "ArcBall.h"

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

class D3DApp//容器，装demo & 运行demo,所以可以组合各demo
{
private:
	WNDCLASSEX mWndClass;
	HWND mhwnd;

	LPCSTR mWndClassName;
	LPCSTR mWndTittleName;

private:
	bool Window_Init(HINSTANCE hInstance,int nShowCmd,WNDPROC wndProc);
	HRESULT  Direct3D_Init(HWND hwnd);
	HRESULT	Direct3D_Render(float timeDelta);


protected:
	LPD3DXFONT mpFont;

protected:
	virtual LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd);
	virtual void Update(float timeDelta);
	virtual void Draw();


public:
	D3DApp(LPCSTR wndClassName,LPCSTR wndTittleName,HINSTANCE hInstance,int nShowCmd,WNDPROC wndProc=0);
	~D3DApp();

	void Run();
};