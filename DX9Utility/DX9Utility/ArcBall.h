#pragma once
#include "Utility.h"

class ArcBall
{
private:
	float _radius;
	float _windowWidth;
	float _windowHeight;


public:
	ArcBall(float windowWidth,float windowHeight);
	~ArcBall();

	D3DXVECTOR3		ScreenToBall(int screenX,int screenY);
	D3DXQUATERNION		QuatFromBallPoints(D3DXVECTOR3 &previousPoint,D3DXVECTOR3 &currentPoint);

};
extern ArcBall* e_pArcball;