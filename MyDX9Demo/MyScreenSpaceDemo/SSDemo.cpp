#include "SSDemo.h"

SSDemo::SSDemo(HINSTANCE hInstance,int nShowCmd):D3DApp("xixi","ScreenSpaceDemo",hInstance,nShowCmd)
{

}

SSDemo::~SSDemo()
{
	SAFE_RELEASE(mGBufferSurface);
	SAFE_RELEASE(mGBuffer);
	SAFE_RELEASE(mLightBufferSurface);
	SAFE_RELEASE(mLightBuffer);
	SAFE_RELEASE(mSSAOSurface);
	SAFE_RELEASE(mSSAOMap);
	SAFE_RELEASE(mForBlurSurface);
	SAFE_RELEASE(mForBlur);
	SAFE_RELEASE(mBackBufferRenderTarget);
	SAFE_RELEASE(mFullScreenQuad);
	SAFE_RELEASE(mQuadDecl);
	SAFE_RELEASE(mFullScreenQuadPlus);
	SAFE_RELEASE(mQuadPlusDecl);
	SAFE_RELEASE(mBaseSceneMesh);
	for(int i=0;i<mBaseSceneTextures.size();i++)
	{
		SAFE_RELEASE(mBaseSceneTextures[i]);
	}
	SAFE_RELEASE(mSkullMesh);
	for(int i=0;i<mSkullTextures.size();i++)
	{
		SAFE_RELEASE(mSkullTextures[i]);
	}
	SAFE_RELEASE(mFX);
}

void SSDemo::CreateTexture()
{
	e_pd3dDevice->GetRenderTarget(0,&mBackBufferRenderTarget);
	D3DXCreateTexture(e_pd3dDevice, WINDOW_WIDTH, WINDOW_HEIGHT, 1, D3DUSAGE_RENDERTARGET, 
		D3DFMT_A16B16G16R16 , D3DPOOL_DEFAULT, &mGBuffer);
	mGBuffer->GetSurfaceLevel(0,&mGBufferSurface);
	D3DXCreateTexture(e_pd3dDevice, WINDOW_WIDTH, WINDOW_HEIGHT, 1, D3DUSAGE_RENDERTARGET, 
		D3DFMT_A16B16G16R16 , D3DPOOL_DEFAULT, &mLightBuffer);
	mLightBuffer->GetSurfaceLevel(0,&mLightBufferSurface);
	D3DXCreateTexture(e_pd3dDevice, WINDOW_WIDTH, WINDOW_HEIGHT, 1, D3DUSAGE_RENDERTARGET, 
		D3DFMT_R16F , D3DPOOL_DEFAULT, &mSSAOMap);//待尝试半分辨率，然后用龚大那个概率分布函数
	mSSAOMap->GetSurfaceLevel(0,&mSSAOSurface);
	D3DXCreateTexture(e_pd3dDevice, WINDOW_WIDTH, WINDOW_HEIGHT, 1, D3DUSAGE_RENDERTARGET, 
		D3DFMT_R16F , D3DPOOL_DEFAULT, &mForBlur);
	mForBlur->GetSurfaceLevel(0,&mForBlurSurface);

}

void SSDemo::CreateQuad()
{
	//full screen quad
	e_pd3dDevice->CreateVertexDeclaration(VertexPTElements,&mQuadDecl);
	e_pd3dDevice->CreateVertexBuffer(4*sizeof(VertexPT),D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &mFullScreenQuad, 0);
	VertexPT* vpt=nullptr;
	mFullScreenQuad->Lock(0,0,(void**)&vpt,0);
	vpt[0].pos=D3DXVECTOR3(-1,1,0);
	vpt[0].tex0=D3DXVECTOR2(0,0);
	vpt[1].pos=D3DXVECTOR3(1,1,0);
	vpt[1].tex0=D3DXVECTOR2(1,0);
	vpt[2].pos=D3DXVECTOR3(1,-1,0);
	vpt[2].tex0=D3DXVECTOR2(1,1);
	vpt[3].pos=D3DXVECTOR3(-1,-1,0);
	vpt[3].tex0=D3DXVECTOR2(0,1);
	mFullScreenQuad->Unlock();

	//full screen quad plus
	e_pd3dDevice->CreateVertexDeclaration(VertexPNTElements,&mQuadPlusDecl);
	e_pd3dDevice->CreateVertexBuffer(4*sizeof(VertexPNT),D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &mFullScreenQuadPlus, 0);
	VertexPNT* vpnt=nullptr;
	mFullScreenQuadPlus->Lock(0,0,(void**)&vpnt,0);
	vpnt[0].pos=D3DXVECTOR3(-1,1,0);
	vpnt[0].normal=D3DXVECTOR3(0,0,0);
	vpnt[0].tex0=D3DXVECTOR2(0,0);
	vpnt[1].pos=D3DXVECTOR3(1,1,0);
	vpnt[1].normal=D3DXVECTOR3(1,0,0);
	vpnt[1].tex0=D3DXVECTOR2(1,0);
	vpnt[2].pos=D3DXVECTOR3(1,-1,0);
	vpnt[2].normal=D3DXVECTOR3(2,0,0);
	vpnt[2].tex0=D3DXVECTOR2(1,1);
	vpnt[3].pos=D3DXVECTOR3(-1,-1,0);
	vpnt[3].normal=D3DXVECTOR3(3,0,0);
	vpnt[3].tex0=D3DXVECTOR2(0,1);
	mFullScreenQuadPlus->Unlock();

	//build frustum far corners
	float farz=e_pCamera->GetFarZ();
	float halfFarHeight=farz*tanf(e_pCamera->GetFovy()*0.5f);
	float halfFarWidth=e_pCamera->GetAspect()*halfFarHeight;
	mFrustumFarCorners[0]=D3DXVECTOR3(-halfFarWidth,halfFarHeight,farz);
	mFrustumFarCorners[1]=D3DXVECTOR3(halfFarWidth,halfFarHeight,farz);
	mFrustumFarCorners[0]=D3DXVECTOR3(halfFarWidth,-halfFarHeight,farz);
	mFrustumFarCorners[0]=D3DXVECTOR3(-halfFarWidth,-halfFarHeight,farz);

	//build offset vectors
	//8 cube corners
	mOffsetvectors[0]=D3DXVECTOR3(1,1,1);
	mOffsetvectors[1]=D3DXVECTOR3(1,1,-1);
	mOffsetvectors[2]=D3DXVECTOR3(1,-1,1);
	mOffsetvectors[3]=D3DXVECTOR3(1,-1,-1);
	mOffsetvectors[4]=D3DXVECTOR3(-1,1,1);
	mOffsetvectors[5]=D3DXVECTOR3(-1,1,-1);
	mOffsetvectors[6]=D3DXVECTOR3(-1,-1,1);
	mOffsetvectors[7]=D3DXVECTOR3(-1,-1,-1);
	//6 centers of cube faces
	mOffsetvectors[8]=D3DXVECTOR3(0,0,-1);
	mOffsetvectors[9]=D3DXVECTOR3(0,0,1);
	mOffsetvectors[10]=D3DXVECTOR3(1,0,0);
	mOffsetvectors[11]=D3DXVECTOR3(-1,0,0);
	mOffsetvectors[12]=D3DXVECTOR3(0,1,0);
	mOffsetvectors[13]=D3DXVECTOR3(0,-1,0);
}


void SSDemo::BuildFX()
{
	ID3DXBuffer *errors;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "ScreenSpace.fx",
		0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if(errors)
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	//render g-buffer
	mhMatWVP=mFX->GetParameterByName(0,"gWVP");
	mhMatWV=mFX->GetParameterByName(0,"gWV");
	mhMatWITV=mFX->GetParameterByName(0,"gWITV");
	mhSpecularPower=mFX->GetParameterByName(0,"gSpecularPower");
	//render light-buffer
	mhActualLightCount=mFX->GetParameterByName(0,"gActualLightCount");
	mhLights=mFX->GetParameterByName(0,"gLights");
	mhGBuffer=mFX->GetParameterByName(0,"gGBuffer");
	mhFrustumCorners=mFX->GetParameterByName(0,"gFrustumCorners");
	mhMatView=mFX->GetParameterByName(0,"gViewMatrix");
	//render SSAO map
	mhOffsetVectors=mFX->GetParameterByName(0,"gOffsetVectors");
	mhRandomVector=mFX->GetParameterByName(0,"gRandomVecotor");
	mhMatProj=mFX->GetParameterByName(0,"gProjMatrix");
	//blur SSAO map
	mhSrcTexture=mFX->GetParameterByName(0,"gSrcTex");
	mhUVperPixel=mFX->GetParameterByName(0,"gUVPerPixel");
	//official rendering
	mhMtrl=mFX->GetParameterByName(0,"gMtrl");
	mhColorTexture=mFX->GetParameterByName(0,"gDiffuseMap");
	mhLightBuffer=mFX->GetParameterByName(0,"gLightBuffer");
	mhSSAOMap=mFX->GetParameterByName(0,"gSSAOMap");
	mhAmbientLight=mFX->GetParameterByName(0,"gAmbientLight");

	//传给gpu固定的：
	mFX->SetInt(mhActualLightCount,mActualLightCount);
	mFX->SetValue(mhLights,mLights,sizeof(mLights));
	mFX->SetValue(mhFrustumCorners,mFrustumFarCorners,sizeof(mFrustumFarCorners));
	mFX->SetValue(mhOffsetVectors,mOffsetvectors,sizeof(mOffsetvectors));
	D3DXMATRIX matProj;
	e_pCamera->GetProjMatrix(matProj);
	mFX->SetMatrix(mhMatProj,&matProj);
	D3DXVECTOR2 UVperPixel=D3DXVECTOR2(1.0f/WINDOW_WIDTH,1.0f/WINDOW_HEIGHT);
	mFX->SetValue(mhUVperPixel,&UVperPixel,sizeof(UVperPixel));
	mFX->SetValue(mhAmbientLight,&mAmbientLight,sizeof(mAmbientLight));
}


void SSDemo::InitLight()
{
	mAmbientLight=D3DXCOLOR(0.18,0.18,0.18,1.0f);
	mLights[0].type=LIGHT_TYPE_DIRECTIONAL;
	mLights[0].dirW=D3DXVECTOR3(1,-2,1);
	D3DXVec3Normalize(&mLights[0].dirW,&mLights[0].dirW);
	//mLights[0].lightColor=D3DXCOLOR(0.6,0.6,0.38,1.0f);
	mLights[0].lightColor=D3DXCOLOR(0.15,0.15,0.15,1.0f);
	//mLights[0].lightColor=D3DXCOLOR(0.05,0.05,0.05,1.0f);
	mLights[1].type=LIGHT_TYPE_POINT;
	mLights[1].posW=D3DXVECTOR3(10,40,0);
	mLights[1].distAttenuation=D3DXVECTOR3(0,0,1);
	mLights[1].lightColor=D3DXCOLOR(0,0.9,0,1.0f);
	mLights[2].type=LIGHT_TYPE_SPOT;
	mLights[2].posW=D3DXVECTOR3(0,50,-20);
	mLights[2].distAttenuation=D3DXVECTOR3(0,0,1);
	mLights[2].dirW=D3DXVECTOR3(0,-1,0);
	D3DXVec3Normalize(&mLights[2].dirW,&mLights[2].dirW);
	mLights[2].spotAngle=D3DXVECTOR3(D3DX_PI/20.0f,D3DX_PI/8.0f,1.0f);
	mLights[2].lightColor=D3DXCOLOR(0.9,0,0,1.0f);
	mActualLightCount=3;
}

void SSDemo::InitSceneGeometry()
{
	LoadXFile("BasicColumnScene/BasicColumnScene.x",&mBaseSceneMesh,mBaseSceneMtrls,mBaseSceneTextures);
	D3DXMatrixIdentity(&mBaseSceneWorld);
	LoadXFile("skullocc.x",&mSkullMesh,mSkullMtrls,mSkullTextures);
	D3DXMatrixTranslation(&mSkullWorld,0,10,0);

}

LRESULT  SSDemo::Object_Init(HINSTANCE hInstance,HWND hwnd)
{
	D3DApp::Object_Init(hInstance,hwnd);
	CreateTexture();
	CreateQuad();
	InitLight();
	BuildFX();
	InitSceneGeometry();
	return S_OK;
}

void SSDemo::Update(float timeDelta)
{
	D3DApp::Update(timeDelta);
}

void SSDemo::Draw()
{
	D3DXMATRIX matView;
	D3DXMATRIX matViewProj;
	e_pCamera->GetViewMatrix(matView);
	e_pCamera->GetViewProj(matViewProj);
	UINT numPasses=0;

	//render g-buffer
	mFX->SetTechnique("GeometryTech");
	e_pd3dDevice->SetRenderTarget(0,mGBufferSurface);
	e_pd3dDevice->Clear(0L, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_RGBA(0, 0, 0, 0), 1.0f, 0L);
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);

	//draw base scene mesh
	HR(mFX->SetMatrix(mhMatWV, &(mBaseSceneWorld*matView)));
	HR(mFX->SetMatrix(mhMatWITV, &(mBaseSceneWorld*matView)));
	HR(mFX->SetMatrix(mhMatWVP, &(mBaseSceneWorld*matViewProj)));	
	for(UINT j = 0; j < mBaseSceneMtrls.size(); ++j)
	{
		mFX->SetFloat(mhSpecularPower,mBaseSceneMtrls[j].specPower);
		mFX->CommitChanges();
		mBaseSceneMesh->DrawSubset(j);
	}

	//draw skull
	HR(mFX->SetMatrix(mhMatWV, &(mSkullWorld*matView)));
	HR(mFX->SetMatrix(mhMatWITV, &(mSkullWorld*matView)));
	HR(mFX->SetMatrix(mhMatWVP, &(mSkullWorld*matViewProj)));
	mFX->SetFloat(mhSpecularPower,mSkullMtrl.specPower);
	mFX->CommitChanges();
	mSkullMesh->DrawSubset(0);

	mFX->EndPass();
	mFX->End();



	//render light buffer
	mFX->SetMatrix(mhMatView,&matView);
	mFX->SetTexture(mhGBuffer,mGBuffer);
	mFX->SetTechnique("LightBufferTech");
	e_pd3dDevice->SetRenderTarget(0,mLightBufferSurface);
	//draw全屏quad并且没开blend，直接覆盖不需要clear
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	//draw full screen quad plus
	e_pd3dDevice->SetVertexDeclaration(mQuadPlusDecl);
	e_pd3dDevice->SetStreamSource(0,mFullScreenQuadPlus,0,sizeof(VertexPNT));
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mFX->EndPass();
	mFX->End();



	//rendr SSAO map
	D3DXVECTOR3 randomVector;
	GetRandomVector(randomVector,D3DXVECTOR3(-2,-2,-2),D3DXVECTOR3(2,2,2));
	D3DXVec3Normalize(&randomVector,&randomVector);
	mFX->SetValue(mhRandomVector,&randomVector,sizeof(D3DXVECTOR3));
	mFX->SetTechnique("SSAOTech");
	e_pd3dDevice->SetRenderTarget(0,mSSAOSurface);
	//draw全屏quad并且没开blend，直接覆盖不需要clear
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	//draw full screen quad plus
	//e_pd3dDevice->SetVertexDeclaration(mQuadPlusDecl);
	//e_pd3dDevice->SetStreamSource(0,mFullScreenQuadPlus,0,sizeof(VertexPNT));
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mFX->EndPass();
	mFX->End();



	//blur SSAO map
	//horizontal
	mFX->SetTechnique("HorizontalBlur");
	mFX->SetTexture(mhSrcTexture,mSSAOMap);
	e_pd3dDevice->SetRenderTarget(0,mForBlurSurface);
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	//draw full screen quad
	e_pd3dDevice->SetVertexDeclaration(mQuadDecl);
	e_pd3dDevice->SetStreamSource(0,mFullScreenQuad,0,sizeof(VertexPT));
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mFX->EndPass();
	mFX->End();
	//vertical
	mFX->SetTechnique("VerticalBlur");
	mFX->SetTexture(mhSrcTexture,mForBlur);
	e_pd3dDevice->SetRenderTarget(0,mSSAOSurface);
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	//draw full screen quad
	//e_pd3dDevice->SetVertexDeclaration(mQuadDecl);
	//e_pd3dDevice->SetStreamSource(0,mFullScreenQuad,0,sizeof(VertexPT));
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mFX->EndPass();
	mFX->End();



	//official rendering!
	e_pd3dDevice->SetRenderTarget(0,mBackBufferRenderTarget);
	mFX->SetTexture(mhLightBuffer,mLightBuffer);
	mFX->SetTexture(mhSSAOMap,mSSAOMap);
	//skull without color texture
	mFX->SetTechnique("OfficialTech");
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	HR(mFX->SetMatrix(mhMatWV, &(mSkullWorld*matView)));
	HR(mFX->SetMatrix(mhMatWITV, &(mSkullWorld*matView)));
	HR(mFX->SetMatrix(mhMatWVP, &(mSkullWorld*matViewProj)));
	mFX->SetValue(mhMtrl,&mSkullMtrl,sizeof(Mtrl));
	mFX->CommitChanges();
	mSkullMesh->DrawSubset(0);
	mFX->EndPass();
	mFX->End();
	//base scene with color texture
	mFX->SetTechnique("Official2Tech");
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	HR(mFX->SetMatrix(mhMatWV, &(mBaseSceneWorld*matView)));
	HR(mFX->SetMatrix(mhMatWITV, &(mBaseSceneWorld*matView)));
	HR(mFX->SetMatrix(mhMatWVP, &(mBaseSceneWorld*matViewProj)));	
	for(UINT j = 0; j < mBaseSceneMtrls.size(); ++j)
	{
		mFX->SetValue(mhMtrl,&mBaseSceneMtrls[j],sizeof(Mtrl) );
		if(mBaseSceneTextures[j] != 0)
		{
			HR(mFX->SetTexture(mhColorTexture, mBaseSceneTextures[j]));
		}
		else
		{
			HR(mFX->SetTexture(mhColorTexture, e_pWhiteTexture));
		}
		mFX->CommitChanges();
		mBaseSceneMesh->DrawSubset(j);
	}
	mFX->EndPass();
	mFX->End();
	
	//debug用
	/*mFX->SetTechnique("DebugTech");
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	//draw full screen quad
	e_pd3dDevice->SetVertexDeclaration(mQuadDecl);
	e_pd3dDevice->SetStreamSource(0,mFullScreenQuad,0,sizeof(VertexPT));
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mFX->EndPass();
	mFX->End();*/


	
}

