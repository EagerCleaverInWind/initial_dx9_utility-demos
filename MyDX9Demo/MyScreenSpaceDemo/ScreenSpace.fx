//render g-buffer
//uniform extern float4x4 gWorld;
//uniform extern float4x4 gWorldInvTrans;
uniform extern float4x4 gWITV;
uniform extern float4x4 gWV;
uniform extern float4x4 gWVP;
uniform extern float gSpecularPower;


//without normal mapping version
void GeometryVS(float3 posL : POSITION0,
	float3 normalL : NORMAL0,
	out float4 posH :POSITION0,
	out float depthV :TEXCOORD0,
	out float3 normalV :TEXCOORD1)
{
	posH=mul(float4(posL,1.0f),gWVP);
	depthV=mul(float4(posL,1.0f),gWV).z;
	normalV=mul(float4(normalL,0.0f),gWITV).xyz;
}

float4 GeometryPS(float depthV :TEXCOORD0,
	float3 normalV :TEXCOORD1):COLOR
{
	float4 output=float4(0,0,0,0);
	normalV=normalize(normalV);
	output.r=normalV.x;
	output.g=normalV.y;
	output.b=depthV;
	output.a=gSpecularPower;
	return output;
}

technique GeometryTech
{
	pass P0
	{
		vertexShader = compile vs_3_0 GeometryVS();
       		pixelShader  = compile ps_3_0 GeometryPS();	
	}

}











//render light buffer
#define  LIGHT_TYPE_DIRECTIONAL 0
#define  LIGHT_TYPE_POINT 1
#define  LIGHT_TYPE_SPOT 2
struct UniformLight
{
	int type;

	float4 lightColor;

	float3 dirW;  
	float3 posW;
	float3 spotAngle;//min_angle,max_anglel,power
	float3 distAttenuation;
};
#define MAX_LIGHT_COUNT 10
uniform extern int gActualLightCount;
uniform extern UniformLight gLights[MAX_LIGHT_COUNT];//CPU中的数组长度要和它一致
uniform extern texture gGBuffer;
sampler GBufferS=sampler_state
{
	Texture = < gGBuffer>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = POINT;
	AddressU  = BORDER;
    	AddressV  = BORDER;
    	BorderColor=float4(0,0,1e5f,0);
};
float3 gFrustumCorners[4];//to reconstruct view space position
//uniform extern float3   gEyePosW;//lighting calculation在view space中，不需要
uniform extern float4x4 gViewMatrix;

void LitVS(float3 posL :POSITION,
	float2 tex0 :TEXCOORD0,
	//float toFarPlaneIndex :TEXCOORD1,
	//懒得新增顶点格式
	float3 toFarPlaneIndex :NORMAL0,
	out float4 posH :POSITION,
	out float2 oTex0 :TEXCOORD0,
	out float3 toFarPlane :TEXCOORD1)
{
	posH=float4(posL,1.0f);
	oTex0=tex0;
	toFarPlane=gFrustumCorners[toFarPlaneIndex.x];
}

float CalculateSpotFactor(float3 lightPos,float3 spotDirection,float3 spotAngle,float3 objectPos)
{
	float spot=1.0f;
	float3 lightVec=normalize(lightPos-objectPos);
	float theta=acos(dot(-lightVec,spotDirection));
	if(theta<=spotAngle.y)
	{
		if(theta>spotAngle.x)
		{
			spot=pow(saturate( cos(3.14159/(2*spotAngle.y)*theta) ),spotAngle.z);
		}
		else
		{
			float threshold=pow(saturate( cos(3.14159/(2*spotAngle.y)*spotAngle.x) ),spotAngle.z);
			spot=threshold*(1.0f- pow(saturate( cos(3.14159/(2*spotAngle.x)*theta) ),spotAngle.z) );
		}

	}
	else
	{
		spot=0.0f;	
	}

	return spot;
}

//唉，支持multiple render targets就好了，light buffer & SSAO map一起计算
float4 LitPS(float2 tex0 :TEXCOORD0, float3 toFarPlane :TEXCOORD1):COLOR
{
	float4 compressCol=tex2D(GBufferS, tex0);
	//empty pixel
	if(compressCol.b==0.0f && compressCol.a==0.0f)
		return float4(0,0,0,0);
	//extract full normalV
	float3 normalV;
	float2 normalVxy=compressCol.rg;
	normalV.xy=normalVxy;
	normalV.z=-sqrt(1.0f-dot(normalVxy,normalVxy));
	//extract full posV
	float3 posV=(compressCol.b/toFarPlane.z)*toFarPlane;
	//calculate lighting!
	float3 diffuseTerm=float3(0,0,0);
	float3 specularTerm=float3(0,0,0);
	for(int i=0;i<gActualLightCount;i++)
	{
		//没办法，shader中的循环终止条件不能用变量——》3_0开始可以了
		//if(i>=gActualLightCount)
			//break;

		float attenuation=1.0f;
		float3 lightVec;
		//gLights[i].dirW=normalize(gLights[i].dirW);//交给cpu吧
		/*float dist;
		switch(gLights[i].type)
		{
			case LIGHT_TYPE_DIRECTIONAL:
				lightVec=mul(-float4(gLights[i].dirW,0.0f), gViewMatrix).xyz;
				break;

			case LIGHT_TYPE_POINT:
				lightVec=mul(float4(gLights[i].posW,1.0f),gViewMatrix).xyz-posV;
				dist=length(lightVec);
				lightVec/=dist;
				//distance attenuation factor
				attenuation*=1.0f/(gLights[i].distAttenuation.x+
					gLights[i].distAttenuation.y*dist+
					gLights[i].distAttenuation.z*dist*dist);
				break;

			case LIGHT_TYPE_SPOT:
				lightVec=mul(float4(gLights[i].posW,1.0f),gViewMatrix).xyz-posV;
				dist=length(lightVec);
				lightVec/=dist;
				//distance attenuation factor
				attenuation*=1.0f/(gLights[i].distAttenuation.x+
					gLights[i].distAttenuation.y*dist+
					gLights[i].distAttenuation.z*dist*dist);
				//spot light attenuation factor
				float theta=acos( dot(-lightVec,mul(float4(gLights[i].dirW,0.0f), gViewMatrix).xyz) );
				attenuation*=pow(cos(3.14159/(2*gLights[i].spotAngle.y)*clamp(theta,gLights[i].spotAngle.x,gLights[i].spotAngle.y))
					,gLights[i].spotAngle.z);
				break;

		}*/
		if(gLights[i].type==LIGHT_TYPE_DIRECTIONAL)
		{
			lightVec=mul(-float4(gLights[i].dirW,0.0f), gViewMatrix).xyz;
		}
		else if(gLights[i].type==LIGHT_TYPE_POINT)
		{
			lightVec=mul(float4(gLights[i].posW,1.0f),gViewMatrix).xyz-posV;
			float dist=length(lightVec);
			lightVec/=dist;
			//distance attenuation factor
			attenuation*=100.0f/dot(gLights[i].distAttenuation,float3(1.0f,dist,dist*dist));//本来是1.0f的，100.0f待整合进光源intensity中
		}
		else//LIGHT_TYPE_SPOT
		{
			float3 lightPosV=mul(float4(gLights[i].posW,1.0f),gViewMatrix).xyz;
			lightVec=lightPosV-posV;
			float dist=length(lightVec);
			lightVec/=dist;
			//distance attenuation factor
			//attenuation*=100.0f/dot(gLights[i].distAttenuation,float3(1.0f,dist,dist*dist));//本来是1.0f的，100.0f待整合进光源intensity中
			//spot light attenuation factor
			//float theta=acos( dot(-lightVec,mul(float4(gLights[i].dirW,0.0f), gViewMatrix).xyz) );
			/*if(theta<gLights[i].spotAngle.y)
			{
				attenuation*=pow(saturate( cos(3.14159/(2*gLights[i].spotAngle.y)*max(theta,gLights[i].spotAngle.x)) )
					,gLights[i].spotAngle.z);
			}
			else
			{
				attenuation*=0;
			}*/
			//循环中if嵌套会编译失败，试试改成函数调用
			//attenuation*=pow(saturate(cos(theta)),8);//特么不知道出了什么问题！老是调不出地面上的范围圆！
			float3 spotDirectionV=mul(float4(gLights[i].dirW,0.0f), gViewMatrix).xyz;
			attenuation*= CalculateSpotFactor(lightPosV,spotDirectionV,gLights[i].spotAngle,posV);
			//嗯，改成函数后编译通过，但依旧调不出圆，
			//原因是，不知怎的是否发光作用在物体会受camera位置影响？点光源也是如此。。。
			//传统渲染没问题，难道是deferred rendering的问题？！
		}

		float nDotL = saturate( dot( normalV,lightVec) );
		//diffuse&specular 待整合进shadow[i] modification factor
		diffuseTerm+=gLights[i].lightColor.rgb*attenuation*nDotL;
		//使用的是normalized Blinn-Phong BRDF,哈哈靠近PBR逼格就是高！
		float specularPower=compressCol.a;
		float3 viewVecV=float3(0,0,0)-posV;
		float3 halfVec=normalize(lightVec+viewVecV);
		//float brdf=(specularPower+2)/8.0f*pow(saturate(dot(normalV,halfVec) ),specularPower);
		float brdf=(specularPower+8)/8.0f*pow(saturate(dot(normalV,halfVec) ),specularPower);
		specularTerm+=brdf*gLights[i].lightColor.rgb*attenuation*nDotL;
	}
	//唉，没有multiple render targets，只好委屈金属了
	float specularTermIntensity= 0.299*specularTerm.r + 0.587*specularTerm.g + 0.114*specularTerm.b;
	return float4(diffuseTerm,specularTermIntensity);

}


technique LightBufferTech
{
	pass p0
	{
		vertexShader = compile vs_3_0 LitVS();
       		pixelShader  = compile ps_3_0 LitPS();

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}











//render SSAO map
#define SAMPLE_COUNT 14
uniform extern float3 gOffsetVectors[SAMPLE_COUNT];
float gOcclusionRadius=0.5f;
float gOcculsionFadeStart=0.2f;
float gOcclusionFadeEnd=2.0f;
float gSurfaceEpsilon=0.05f;
//uniform extern texture gNormalDepthMap;//render light buffer时已经有了
//uniform extern texture gRandomVecMap;
//没有随机vector纹理
uniform extern float3 gRandomVecotor;
uniform extern float4x4 gProjMatrix;

float4 SSAOPS(float2 tex0 :TEXCOORD0, float3 toFarPlane :TEXCOORD1):COLOR
{
	float4 compressCol=tex2D(GBufferS, tex0);
	//empty pixel
	if(compressCol.b==0.0f && compressCol.a==0.0f)
		return float4(0,0,0,0);
	//extract full normalV
	float3 normalV;
	float2 normalVxy=compressCol.rg;
	normalV.xy=normalVxy;
	normalV.z=-sqrt(1.0f-dot(normalVxy,normalVxy));
	//extract full posV
	float3 posV=(compressCol.b/toFarPlane.z)*toFarPlane;

	float occlusionSum=0.0f;
	for(int i=0;i<SAMPLE_COUNT;i++)
	{
		float3 offset=reflect(gOffsetVectors[i], gRandomVecotor);
		float flip=sign(dot(offset,normalV));
		float3 samplePoint=posV+flip*gOcclusionRadius*offset;
		float4 sampleProj=mul(float4(samplePoint,1.0f),gProjMatrix);
		sampleProj/=sampleProj.w;
		float2 sampleProjUV=(sampleProj.xy+1)*0.5f;
		float3 occlusionPoint;
		//find occlusion point
		occlusionPoint.z=tex2D(GBufferS,sampleProjUV ).b;
		occlusionPoint=(occlusionPoint.z/samplePoint.z)*samplePoint;
		float3 POVec=occlusionPoint-posV;
		float dist=length(POVec);
		POVec/=dist;
		float occlusion=0.0f;
		if(dist>gSurfaceEpsilon)
		{
			occlusion=saturate( (gOcclusionFadeEnd-dist)/(gOcclusionFadeEnd- gOcculsionFadeStart) );
		}
		float occlusionWeight=max(dot(POVec, normalV), 0.0f);
		occlusionSum+=occlusionWeight*occlusion;
	}
	occlusionSum/=SAMPLE_COUNT;
	float access=1.0f- occlusionSum;

	//sharpen锐化，两极更加明显
	access=saturate(pow(access,4.0f) );
	return float4(access,0,0,0);

}

technique SSAOTech
{
	pass p0
	{
		vertexShader = compile vs_3_0 LitVS();
       		pixelShader  = compile ps_3_0 SSAOPS();

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}












//blur SSAO map,not simple Gaussian but the edge preserving blur,edges are detected from the normal/depth map
uniform extern texture gSrcTex;
sampler SrcS=sampler_state
{
	Texture = <gSrcTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter=POINT;
	AddressU  = CLAMP;
    	AddressV  = CLAMP;
};
uniform extern float2 gUVPerPixel;
static const float gWeights[11]={0.05f, 0.05f, 0.1f, 0.1f, 0.1f, 0.2f, 0.1f, 0.1f, 0.1f, 0.05f, 0.05f};
static const int gBlurRadius=5;

void QuadVS(float3 posL :POSITION,
	float2 tex0 :TEXCOORD0,
	out float4 posH :POSITION,
	out float2 oTex0 :TEXCOORD0)
{
	posH=float4(posL,1.0f);
	oTex0=tex0;
}

float4 EdgePreservingBlurPS(float2 tex0 :TEXCOORD0, uniform bool gHorizontalBlur):COLOR
{
	float4 compressCol=tex2D(GBufferS, tex0);
	//empty pixel
	if(compressCol.b==0.0f && compressCol.a==0.0f)
		return float4(0,0,0,0);
	//extract full normalV
	float3 normalV;
	float2 normalVxy=compressCol.rg;
	normalV.xy=normalVxy;
	normalV.z=-sqrt(1.0f-dot(normalVxy,normalVxy));

	float2 texOffset;
	if(gHorizontalBlur)
		texOffset=float2(gUVPerPixel.x,0);
	else
		texOffset=float2(0,gUVPerPixel.y);

	float colorSum=gWeights[5]*tex2D(SrcS, tex0).r;
	float totalWeight=gWeights[5];
	for(int i=-gBlurRadius;i<=gBlurRadius;i++)
	{
		if(i==0)
			continue;

		float2 tex=tex0+i*texOffset;
		float4 neighborCol=tex2D(GBufferS, tex);
		//extract full normalV
		float3 neighborNormal;
		float2 neighborNormalxy=neighborCol.rg;
		neighborNormal.xy=neighborNormalxy;
		neighborNormal.z=-sqrt(1.0f-dot(neighborNormalxy,neighborNormalxy));
		if(dot(neighborNormal,normalV)>=0.8f && abs(neighborCol.z- compressCol.z)<=0.2f)
		{
			float weight=gWeights[i+gBlurRadius] ;
			colorSum+=weight*tex2D(SrcS, tex).r;
			totalWeight+=weight;
		}

	}

	//return colorSum/totalWeight;
	return float4(colorSum/totalWeight,0,0,0);

}

technique HorizontalBlur
{
	pass p0
	{
		vertexShader = compile vs_3_0 QuadVS();
       		pixelShader  = compile ps_3_0 EdgePreservingBlurPS(true);

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
	
}

technique VerticalBlur
{
	pass p0
	{
		vertexShader = compile vs_3_0 QuadVS();
       		pixelShader  = compile ps_3_0 EdgePreservingBlurPS(false);

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
	
}















//终于要正式渲染啦！
struct Mtrl
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float  specPower;
};
uniform extern Mtrl     gMtrl;
uniform extern texture  gDiffuseMap;//也就是最基本的color texture
sampler DiffuseAlbedoS = sampler_state
{
	Texture = <gDiffuseMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
    	AddressV  = WRAP;
};
uniform extern texture gLightBuffer;
sampler LightBufferS= sampler_state
{
	Texture = <gLightBuffer>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter=POINT;
	AddressU  = CLAMP;
    	AddressV  = CLAMP;
};
uniform extern texture gSSAOMap;
sampler SSAOS=sampler_state
{
	Texture = <gSSAOMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter=POINT;
	AddressU  = CLAMP;
    	AddressV  = CLAMP;
};
uniform extern float4 gAmbientLight;

//no color texture & no reflectance version
 void OfficialVS(float3 posL :POSITION,
 	float3 normalL :NORMAL0,
 	out float4 posH :POSITION,
 	out float2 projTex :TEXCOORD0,
 	out float3 normalV :TEXCOORD1,
 	out float3 posV :TEXCOORD2)
 {
 	posH=mul(float4(posL,1.0f),gWVP);
 	//projTex=(posH.xy/posH.w+1.0f)*0.5f;//原来是y轴忘记反了！！！！
 	projTex.x=(posH.x/posH.w+1.0f)*0.5f;
 	projTex.y=(posH.y/posH.w-1.0f)*-0.5f;
 	//just for fresnel term of specular brdf
 	normalV=mul(float4(normalL,0.0f),gWITV).xyz;
 	posV=mul(float4(posL,1.0f),gWV).xyz;
 }

 float4 OfficialPS(float2 projTex :TEXCOORD0,float3 normalV :TEXCOORD1,float3 posV :TEXCOORD2):COLOR
{
	float4 modifiedLighting=tex2D(LightBufferS, projTex);
	float ambientAccess=tex2D(SSAOS, projTex).r;
	float3 diffuseCol=modifiedLighting.rgb * gMtrl.diffuse.rgb;
	//float3 specularCol=modifiedLighting.a * gMtrl.spec.rgb;
	//pbr
	normalV=normalize(normalV);
	float3 toEyeV=normalize(0- posV);
	float3 fresnelTerm=gMtrl.spec.rgb+(1-gMtrl.spec.rgb)*pow((1-dot(toEyeV,normalV)),5);
	float3 specularCol=modifiedLighting.a * fresnelTerm;
	float3 ambientCol=gAmbientLight.rgb*ambientAccess * gMtrl.ambient.rgb;
	//float3 ambientCol=gAmbientLight.rgb * gMtrl.ambient.rgb;//试下关闭SSAO
	return float4(diffuseCol+specularCol+ambientCol,gMtrl.diffuse.a);
}

technique OfficialTech
{
	pass p0
	{
		vertexShader = compile vs_3_0 OfficialVS();
       		pixelShader  = compile ps_3_0 OfficialPS();

	      	ZEnable=TRUE;
	      	ZFunc=EQUAL;
        		ZWriteEnable=FALSE;
	}
}




//with color texture version
 void Official2VS(float3 posL :POSITION,
 	float2 tex0 :TEXCOORD0,
 	out float4 posH :POSITION,
 	out float2 diffuseTex :TEXCOORD0,
 	out float2 projTex :TEXCOORD1)
 {
 	posH=mul(float4(posL,1.0f),gWVP);
 	diffuseTex=tex0;
 	//projTex=(posH.xy/posH.w+1.0f)*0.5f;
 	projTex.x=(posH.x/posH.w+1.0f)*0.5f;
 	projTex.y=(posH.y/posH.w-1.0f)*-0.5f;
 }

 float4 Official2PS(float2 diffuseTex :TEXCOORD0,float2 projTex :TEXCOORD1):COLOR
{
	float4 modifiedLighting=tex2D(LightBufferS, projTex);
	float ambientAccess=tex2D(SSAOS, projTex).r;
	float4 diffuseAlbedo=tex2D(DiffuseAlbedoS, diffuseTex);

	float3 diffuseCol=modifiedLighting.rgb * diffuseAlbedo.rgb;
	float3 specularCol=modifiedLighting.a * gMtrl.spec.rgb;
	float3 ambientCol=gAmbientLight.rgb*ambientAccess * gMtrl.ambient.rgb;
	return float4(diffuseCol+specularCol+ambientCol,diffuseAlbedo.a);
}

technique Official2Tech
{
	pass p0
	{
		vertexShader = compile vs_3_0 Official2VS();
       		pixelShader  = compile ps_3_0 Official2PS();

	      	ZEnable=TRUE;
	      	ZFunc=EQUAL;
        		ZWriteEnable=FALSE;
	}
}



//with reflectance version



//debug用
float4 DebugPS(float2 tex0 :TEXCOORD0):COLOR
{
	float4 compressCol=tex2D(GBufferS, tex0);
	float4 lightCol=tex2D(LightBufferS, tex0);
	float SSAOCol=tex2D(SSAOS, tex0).r;

	//extract full normalV
	float3 normalV;
	float2 normalVxy=compressCol.rg;
	normalV.xy=normalVxy;
	normalV.z=-sqrt(1.0f-dot(normalVxy,normalVxy));
	//normal texture
	normalV=(normalV+1.0f)*0.5f;

	//return depth
	//return compressCol.b;

	//return normal
	//return float4(normalV,1.0f);

	//return light buffer
	//return float4(lightCol.rgb+lightCol.a,1.0f);

	//return ssao access
	return SSAOCol;
}
technique DebugTech
{
	pass p0
	{
		vertexShader = compile vs_3_0 QuadVS();
       		pixelShader  = compile ps_3_0 DebugPS();

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}







