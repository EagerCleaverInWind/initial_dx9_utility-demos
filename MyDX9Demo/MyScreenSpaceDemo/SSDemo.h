#include "D3DApp.h"
#define  MAX_LIGHT_COUNT 10

//待整合进SSR & shadow technology，就完美了
class SSDemo:public D3DApp
{
private:
	//kernel
	LPDIRECT3DTEXTURE9 mGBuffer;
	LPDIRECT3DSURFACE9 mGBufferSurface;
	LPDIRECT3DTEXTURE9 mLightBuffer;
	LPDIRECT3DSURFACE9 mLightBufferSurface;
	LPDIRECT3DTEXTURE9 mSSAOMap;
	LPDIRECT3DSURFACE9 mSSAOSurface;
	LPDIRECT3DTEXTURE9 mForBlur;
	LPDIRECT3DSURFACE9 mForBlurSurface;
	LPDIRECT3DSURFACE9 mBackBufferRenderTarget;
	LPDIRECT3DVERTEXBUFFER9 mFullScreenQuad;
	IDirect3DVertexDeclaration9* mQuadDecl;
	LPDIRECT3DVERTEXBUFFER9 mFullScreenQuadPlus;
	IDirect3DVertexDeclaration9* mQuadPlusDecl;
	D3DXVECTOR3 mFrustumFarCorners[4];
	D3DXVECTOR3 mOffsetvectors[14];
	D3DXVECTOR3 mRandomVector;

	//scene geometry
	ID3DXMesh* mBaseSceneMesh;
	D3DXMATRIX mBaseSceneWorld;
	std::vector<Mtrl> mBaseSceneMtrls;
	std::vector<IDirect3DTexture9*> mBaseSceneTextures;
	ID3DXMesh* mSkullMesh;
	D3DXMATRIX mSkullWorld;
	//虽说必空，但为了使用函数。。。
	std::vector<Mtrl> mSkullMtrls;
	std::vector<IDirect3DTexture9*> mSkullTextures;
	Mtrl mSkullMtrl;//Mtrl默认构造函数是WHITE

	//lights
	UniformLight mLights[MAX_LIGHT_COUNT];
	int mActualLightCount;
	D3DXCOLOR mAmbientLight;

	//.fx
	ID3DXEffect* mFX;
	//render g-buffer
	D3DXHANDLE mhMatWVP;
	D3DXHANDLE mhMatWV;
	D3DXHANDLE mhMatWITV;
	D3DXHANDLE mhSpecularPower;
	//render light-buffer
	D3DXHANDLE mhActualLightCount;
	D3DXHANDLE mhLights;
	D3DXHANDLE mhGBuffer;
	D3DXHANDLE mhFrustumCorners;
	D3DXHANDLE mhMatView;
	//render SSAO map
	D3DXHANDLE mhOffsetVectors;
	D3DXHANDLE mhRandomVector;
	D3DXHANDLE mhMatProj;
	//blur SSAO map
	D3DXHANDLE mhSrcTexture;
	D3DXHANDLE mhUVperPixel;
	//official rendering
	D3DXHANDLE mhMtrl;
	D3DXHANDLE mhColorTexture;
	D3DXHANDLE mhLightBuffer;
	D3DXHANDLE mhSSAOMap;
	D3DXHANDLE mhAmbientLight;


private:
	void CreateTexture();
	void CreateQuad();//包括declaration & frustum far corners & offset vectors
	void BuildFX();
	void InitLight();//light & material
	void InitSceneGeometry();

	//virtual function from base class
	LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd);
	void Update(float timeDelta);
	void Draw();

public:
	SSDemo(HINSTANCE hInstance,int nShowCmd);
	~SSDemo();

};