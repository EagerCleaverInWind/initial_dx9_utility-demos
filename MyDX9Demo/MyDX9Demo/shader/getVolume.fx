//project mesh
uniform extern float gHalfWidth;
uniform extern float gHalfHeight;
uniform extern float gRefZ;
uniform extern float gHalfCiipDist;
uniform extern float gAreaPerPixel;

void MeshVS(float3 posL :POSITION0,
	out float4 posH :POSITION0,
	out float h :TEXCOORD0)
{
	//直接在local sapce中正交投影
	posH.x=posL.x/gHalfWidth;
	posH.y=posL.y/gHalfHeight;
	posH.z=(posL.z/gHalfCiipDist+1)/2.0f;//强迫症如果非要求的话，其实可以关闭Z test没必要求
	posH.w=1.0f;

	h=posL.z-gRefZ;
}

float4 MeshPS(float h :TEXCOORD0):COLOR
{
	return float4(h*gAreaPerPixel,0,0,0);
}


technique DifferentialMesh
{
	pass P0//front face
	{
		vertexShader = compile vs_3_0 MeshVS();
       		pixelShader  = compile ps_3_0 MeshPS();
        
        		CullMode=CCW;

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}

	pass P1//back face
	{
		vertexShader = compile vs_3_0 MeshVS();
       		pixelShader  = compile ps_3_0 MeshPS();
        
       		CullMode=CW;

        		AlphaBlendEnable = true;
	      	SrcBlend = ONE;
	      	DestBlend =ONE;
	      	BlendOp=SUBTRACT;

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}







//4x4down sample
uniform extern  texture gSrcTex;
sampler TexS=sampler_state
{
	Texture = <gSrcTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter=NONE;
};
uniform extern float2 gUVPerPixel;
static const float gDeviations[4]={-1.5,-0.5,0.5,1.5};

void QuadVS(float3 posL :POSITION,
	float2 tex0 :TEXCOORD0,
	out float4 posH :POSITION,
	out float2 oTex0 :TEXCOORD0)
{
	posH=float4(posL,1.0f);
	oTex0=tex0;
}

float4 SumPS(float2 tex0:TEXCOORD0):COLOR
{
	float sum=0;
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			float2 uv=tex0+float2(i*gUVPerPixel.x , j*gUVPerPixel.y);
			sum+=tex2D(TexS, uv).r;
		}
	}

	return float4(sum,0,0,0);
}

technique DownSample4x4
{
	pass P0
	{
		vertexShader = compile vs_3_0 QuadVS();
       		pixelShader  = compile ps_3_0 SumPS();

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}




