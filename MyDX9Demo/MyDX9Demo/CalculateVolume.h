#pragma  once

#include "utility.h"
//原理：根据Rasterization自带differential
class MeshVolume
{
private:
	ID3DXMesh *mMesh;
	BoundingBox mBoundingBox;
	DWORD mNumSubsets;

	LPDIRECT3DTEXTURE9 mFirstTex;//512x512
	//for 4x4 down sample:
	LPDIRECT3DTEXTURE9 mSecondTex;//128x128
	LPDIRECT3DTEXTURE9 mThirdTex;//32x32
	LPDIRECT3DTEXTURE9 mFourthTex;//8x8,差不多可以在cpu中访问了
	LPDIRECT3DVERTEXBUFFER9 mFullScreenQuad;
	IDirect3DVertexDeclaration9* mQuadDecl;
	float mResult;
	LPDIRECT3DSURFACE9 mBackBufferRenderTarget;
	LPDIRECT3DSURFACE9 mFirstSurface;
	LPDIRECT3DSURFACE9 mSecondSurface;
	LPDIRECT3DSURFACE9 mThirdSurface;
	LPDIRECT3DSURFACE9 mFourthSurface;
	LPDIRECT3DTEXTURE9 mCpuTex;//因为D3DUSAGE_RENDERTARGET不能lock
	LPDIRECT3DSURFACE9 mCpuSurface;


	//直接在local space中正交投影
	float mProjectionWidth;
	float mProjectionHeight;
	float mSizeBias;//投影面尺寸略大于bounding box
	float mZReference;//-(BoundingBox.z/2+bias)
	float mClipDist;
	float mAreaPerPixel;

	ID3DXEffect* mFX;
	D3DXHANDLE mhHalfWidth;
	D3DXHANDLE mhHalfHeight;
	D3DXHANDLE mhRefZ;
	D3DXHANDLE mhHalfClipDist;
	D3DXHANDLE mhAreaPerPixel;
	D3DXHANDLE mhTex;
	D3DXHANDLE mhUVPerPixel;


private:
	void BuildFX();
	void CreateTexture();
	void CreateQuad();

	void Update();
	void Draw();//注意不是放在demo容器的每帧更新+渲染框架中，所以渲染时要自己BeginScene()
	void Calculate();//渲染完8x8后取回cpu中计算最终结果


public:
	MeshVolume();
	~MeshVolume();

	float GetVolume(){return mResult;}
	void SetMesh(ID3DXMesh* mesh);

};