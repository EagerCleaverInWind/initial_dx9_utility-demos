#include "D3DApp.h"
#include "Sky.h"
#include "CalculateVolume.h"

class SkyDemo:public D3DApp
{
private:
	Sky* mpSky;

	LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd)
	{
		D3DApp::Object_Init(hInstance,hwnd);
		mpSky=new Sky("texture/grassenvmap1024.dds");
		return S_OK;
	}

	void Update(float timeDelta)
	{
		D3DApp::Update(timeDelta);

	}

	void Draw()
	{
		D3DApp::Draw();
		mpSky->Draw();
	}
	
public:
	SkyDemo(HINSTANCE hInstance,int nShowCmd):D3DApp("xixi","sky demo",hInstance,nShowCmd)
	{

	}

	~SkyDemo()
	{
		SAFE_DELETE(mpSky);
	}

};

class ComputeVolumeDemo:public D3DApp
{
private:
	MeshVolume* mpMeshVolume;
	ID3DXMesh* mSphere;
	float mSphereVolume;

	LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd)
	{
		D3DApp::Object_Init(hInstance,hwnd);
		mpMeshVolume=new MeshVolume();
		D3DXCreateSphere(e_pd3dDevice,5,100,100,&mSphere,0);
		//D3DXCreateBox(e_pd3dDevice,3,2,1,&mSphere,0);
		//D3DXCreateCylinder(e_pd3dDevice,3,3,5,100,100,&mSphere,0);
		mpMeshVolume->SetMesh(mSphere);
		mSphereVolume=mpMeshVolume->GetVolume();

		return S_OK;
	}

	void Update(float timeDelta)
	{
		D3DApp::Update(timeDelta);

	}

	void Draw()
	{
		D3DApp::Draw();

		static char	showVolume[30];
		RECT  formatRect={0,0,WINDOW_WIDTH,WINDOW_HEIGHT};
		int charCount=sprintf_s(showVolume, _T("sphere volume:%0.5f"),mSphereVolume);
		mpFont->DrawText(NULL,showVolume,charCount,&formatRect,DT_TOP | DT_LEFT, D3DCOLOR_XRGB(180,96,96));
	}

public:
	ComputeVolumeDemo(HINSTANCE hInstance,int nShowCmd):D3DApp("xixi","volume demo",hInstance,nShowCmd)
	{

	}

	~ComputeVolumeDemo()
	{
		SAFE_DELETE(mpMeshVolume);
		SAFE_RELEASE(mSphere);
	}

	
};

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nShowCmd)
{
	//D3DApp app=D3DApp("xixi","haha",hInstance,nShowCmd);
	//SkyDemo app=SkyDemo(hInstance,nShowCmd);
	ComputeVolumeDemo app=ComputeVolumeDemo(hInstance,nShowCmd);
	app.Run();
	return 0;
}
