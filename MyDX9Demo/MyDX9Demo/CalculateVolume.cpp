#include "CalculateVolume.h"

MeshVolume::MeshVolume():mResult(0),mSizeBias(0.4f)
{
	CreateTexture();
	CreateQuad();
	BuildFX();
}

MeshVolume::~MeshVolume()
{
	SAFE_RELEASE(mFirstSurface);
	SAFE_RELEASE(mFirstTex);
	SAFE_RELEASE(mSecondSurface);
	SAFE_RELEASE(mSecondTex);
	SAFE_RELEASE(mThirdSurface);
	SAFE_RELEASE(mThirdTex);
	SAFE_RELEASE(mFullScreenQuad);
	SAFE_RELEASE(mQuadDecl);
	SAFE_RELEASE(mBackBufferRenderTarget);
	SAFE_RELEASE(mFX);
	SAFE_RELEASE(mCpuSurface);
	SAFE_RELEASE(mCpuTex);
}

void MeshVolume::BuildFX()
{
	ID3DXBuffer *errors;
	HR(D3DXCreateEffectFromFile(e_pd3dDevice, "shader/getVolume.fx",
		0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
	if(errors)
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

	mhHalfWidth=mFX->GetParameterByName(0,"gHalfWidth");
	mhHalfHeight=mFX->GetParameterByName(0,"gHalfHeight");
	mhRefZ=mFX->GetParameterByName(0,"gRefZ");
	mhHalfClipDist=mFX->GetParameterByName(0,"gHalfCiipDist");
	mhAreaPerPixel=mFX->GetParameterByName(0,"gAreaPerPixel");
	mhTex=mFX->GetParameterByName(0,"gSrcTex");
	mhUVPerPixel=mFX->GetParameterByName(0," gUVPerPixel");
}

void MeshVolume::CreateTexture()
{
	e_pd3dDevice->GetRenderTarget(0,&mBackBufferRenderTarget);
	D3DXCreateTexture(e_pd3dDevice, 512, 512, 1, D3DUSAGE_RENDERTARGET, 
		D3DFMT_R32F, D3DPOOL_DEFAULT, &mFirstTex);
	mFirstTex->GetSurfaceLevel(0, &mFirstSurface);
	e_pd3dDevice->SetRenderTarget(0, mFirstSurface);
	e_pd3dDevice->Clear(0L, NULL, D3DCLEAR_TARGET, D3DCOLOR_RGBA(0, 0, 0, 0), 1.0f, 0L);
	e_pd3dDevice->SetRenderTarget(0, mBackBufferRenderTarget);

	D3DXCreateTexture(e_pd3dDevice, 128, 128, 1, D3DUSAGE_RENDERTARGET, 
		D3DFMT_R32F, D3DPOOL_DEFAULT, &mSecondTex);
	mSecondTex->GetSurfaceLevel(0, &mSecondSurface);
	D3DXCreateTexture(e_pd3dDevice, 32, 32, 1, D3DUSAGE_RENDERTARGET, 
		D3DFMT_R32F, D3DPOOL_DEFAULT, &mThirdTex);
	mThirdTex->GetSurfaceLevel(0, &mThirdSurface);
	D3DXCreateTexture(e_pd3dDevice, 8, 8, 1, D3DUSAGE_RENDERTARGET, 
		D3DFMT_R32F, D3DPOOL_DEFAULT, &mFourthTex);
	mFourthTex->GetSurfaceLevel(0, &mFourthSurface);

	D3DXCreateTexture(e_pd3dDevice, 8, 8, 1, 0, 
		D3DFMT_R32F, D3DPOOL_SYSTEMMEM, &mCpuTex);
	mCpuTex->GetSurfaceLevel(0, &mCpuSurface);

}

void MeshVolume::CreateQuad()
{
	e_pd3dDevice->CreateVertexDeclaration(VertexPTElements,&mQuadDecl);

	//full screen quad
	e_pd3dDevice->CreateVertexBuffer(4*sizeof(VertexPT),D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &mFullScreenQuad, 0);
	VertexPT* vpt=nullptr;
	mFullScreenQuad->Lock(0,0,(void**)&vpt,0);
	vpt[0].pos=D3DXVECTOR3(-1,1,0);
	vpt[0].tex0=D3DXVECTOR2(0,0);
	vpt[1].pos=D3DXVECTOR3(1,1,0);
	vpt[1].tex0=D3DXVECTOR2(1,0);
	vpt[2].pos=D3DXVECTOR3(1,-1,0);
	vpt[2].tex0=D3DXVECTOR2(1,1);
	vpt[3].pos=D3DXVECTOR3(-1,-1,0);
	vpt[3].tex0=D3DXVECTOR2(0,1);
	mFullScreenQuad->Unlock();
}

void MeshVolume::SetMesh(ID3DXMesh* mesh)
{
	mMesh=mesh;
	Update();
	Draw();
	Calculate();
}

void MeshVolume::Update()
{
	//get boundingbox
	BYTE* vp=nullptr;
	mMesh->LockVertexBuffer(0,(void**)&vp);
	D3DXComputeBoundingBox((D3DXVECTOR3*)vp,mMesh->GetNumVertices(),
		//D3DXGetFVFVertexSize(mMesh->GetFVF()),//这里有待改进
		mMesh->GetNumBytesPerVertex(),
		&mBoundingBox._min,&mBoundingBox._max);
	mMesh->UnlockVertexBuffer();

	//get the number of subsets
	mNumSubsets=0;
	HRESULT hr= mMesh->GetAttributeTable(0, &mNumSubsets);//前提是mesh经过ID3DXMesh::Optimize，才能这样得到子集个数

	//update projection
	mProjectionWidth=(mBoundingBox._max.x+mSizeBias)*2;
	mProjectionHeight=(mBoundingBox._max.y+mSizeBias)*2;
	mAreaPerPixel=mProjectionWidth*mProjectionHeight/(512*512);
	mFX->SetFloat(mhAreaPerPixel,mAreaPerPixel);
	mFX->SetFloat(mhHalfWidth,mBoundingBox._max.x+mSizeBias);
	mFX->SetFloat(mhHalfHeight,mBoundingBox._max.y+mSizeBias);
	mFX->SetFloat(mhHalfClipDist,mBoundingBox._max.z+mSizeBias);
	mFX->SetFloat(mhRefZ,mBoundingBox._min.z-mSizeBias);
}

void MeshVolume::Draw()
{
	//e_pd3dDevice->BeginScene();//base class中调用Draw()的函数中已经有了

	//differential
	mFX->SetTechnique("DifferentialMesh");
	e_pd3dDevice->SetRenderTarget(0, mFirstSurface);
	e_pd3dDevice->Clear(0L, NULL, D3DCLEAR_TARGET, D3DCOLOR_RGBA(0, 0, 0, 0), 1.0f, 0L);
	UINT numPasses=0;
	mFX->Begin(&numPasses,0);
	for(UINT i=0;i<numPasses;i++)
	{
		mFX->BeginPass(i);
		//for(DWORD j=0;j<mNumSubsets;j++)
			//mMesh->DrawSubset(j);
		mMesh->DrawSubset(0);
		mFX->EndPass();
	}
	mFX->End();

	//4x4 down sample
	mFX->SetTechnique("DownSample4x4");
	e_pd3dDevice->SetVertexDeclaration(mQuadDecl);
	e_pd3dDevice->SetStreamSource(0,mFullScreenQuad,0,sizeof(VertexPT));

	e_pd3dDevice->SetRenderTarget(0,mSecondSurface);
	mFX->SetTexture(mhTex,mFirstTex);
	mFX->SetValue(mhUVPerPixel,&D3DXVECTOR2(1/512,1/512),sizeof(D3DXVECTOR2));
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mFX->EndPass();
	mFX->End();

	e_pd3dDevice->SetRenderTarget(0,mThirdSurface);
	mFX->SetTexture(mhTex,mSecondTex);
	mFX->SetValue(mhUVPerPixel,&D3DXVECTOR2(1/128,1/128),sizeof(D3DXVECTOR2));
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mFX->EndPass();
	mFX->End();

	e_pd3dDevice->SetRenderTarget(0,mFourthSurface);
	mFX->SetTexture(mhTex,mThirdTex);
	mFX->SetValue(mhUVPerPixel,&D3DXVECTOR2(1/64,1/64),sizeof(D3DXVECTOR2));
	mFX->Begin(&numPasses,0);
	mFX->BeginPass(0);
	e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
	mFX->EndPass();
	mFX->End();

	//e_pd3dDevice->EndScene();
	e_pd3dDevice->SetRenderTarget(0,mBackBufferRenderTarget);
}

void MeshVolume::Calculate()
{
	e_pd3dDevice->GetRenderTargetData(mFourthSurface,mCpuSurface);
	D3DLOCKED_RECT lockedRect;
	HRESULT hr = mCpuSurface->LockRect( &lockedRect, NULL, 0);
	if(!FAILED(hr))
	{
		float* pData=(float*)lockedRect.pBits;
		mResult=0;
		for(int i=0;i<8;i++)
		{
			for(int j=0;j<8;j++)
			{
				int index=i*lockedRect.Pitch/4+j;
				mResult+=pData[index];
			}
		}
	}
	mCpuSurface->UnlockRect();
}