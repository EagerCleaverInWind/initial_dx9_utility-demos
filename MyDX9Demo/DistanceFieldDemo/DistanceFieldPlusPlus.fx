#include"distanceFieldUtility.fx"
//注释过，发现奇慢不是因为函数多，而是调用嵌套、次数太多

static const float2 gNearClipHalfSize=float2(4.0f/3.0f,1);//投影面在world space中尺寸，不是screen resolution
static const float2 gNearFarDistance=float2(1,5000);
static const float2 gResolution=float2(800,600);
static const float gDistanceEpsilon=0.0001f;
static const float gMaxSteps=30;
//static const int gMaxReflectionTimes=3;
#define MAX_RAY_DEPTH 2

uniform extern float3 gEyePosW;
uniform extern float3 gCamForward;
uniform extern float3 gCamUp;
uniform extern float3 gCamRight;

//light source
static const float3 gSkyColor=float3(0.93,0.73,0.93);
//对于多个punctual light考虑用数组装？
uniform extern float4 gLightVec;//w component，点光源or方向光
uniform extern float4 gLightCol;//a component,距离衰减系数
uniform extern float4 gAmbientLight;
static const float gMaxLightDistance=1000.0f;
uniform extern texture gSkyBox;
sampler SkyBoxS = sampler_state
{
	Texture = <gSkyBox>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
          AddressV  = WRAP;
};

struct PBRMtrl
{
	float4 diffuseAlbedo;//a表metallic,因为这里不需要压缩进texture即specular albedo保留了原本的三个component，所以这个量没有意义了
	float4 specularAlbedo;//a表roughness，算了，还是表示smoothness更intuitive
};





//特定scene，自己组合
uniform extern float4x4 gBlueSphereMat;
uniform extern float4x4 gRedSphereMat;
uniform extern float4x4 gGreenBoxMat;
uniform extern float4x4 gTorus88Mat;

//PS:特么还跟c一样，被调用函数要放在调用它的函数的前面

//特定scene，自己组合
inline float distScene(float3 p)
{
	float4 posW=float4(p,1.0f);
	float blueSphereDist=sdSphere(mul(posW,gBlueSphereMat).xyz,3.0f);
	float redSphereDist=sdSphere(mul(posW,gRedSphereMat).xyz,7.0f);
	float greenBoxDist=udRoundBox(mul(posW,gGreenBoxMat).xyz,float3(7,8,9),1.8f);
	float torus88Dist=sdTorus88(mul(posW,gTorus88Mat).xyz,float2(3,5));

	return min(min(min(blueSphereDist,redSphereDist),greenBoxDist),torus88Dist);
}

//特定scene，自己组合
float distSceneWithMtrl(float3 p,out PBRMtrl mtrl)
{
	float4 posW=float4(p,1.0f);
	float blueSphereDist=sdSphere(mul(posW,gBlueSphereMat).xyz,3.0f);
	float redSphereDist=sdSphere(mul(posW,gRedSphereMat).xyz,7.0f);
	float greenBoxDist=udRoundBox(mul(posW,gGreenBoxMat).xyz,float3(7,8,9),1.8f);
	float torus88Dist=sdTorus88(mul(posW,gTorus88Mat).xyz,float2(3,5));

	float minDist=blueSphereDist;
	//光滑蓝金属
	mtrl.diffuseAlbedo=float4(0,0,0,1);
	mtrl.specularAlbedo=float4(0.1,0.1,0.97,0.5);
	//因为调用频繁且只在最后一次即碰到表面时赋的mtrl才用得上，改为返回mtrl数组索引会更高效一点，shader中的struct应该可以写构造函数吧？
	if(redSphereDist<minDist)
	{
		minDist=redSphereDist;
		//粗糙红塑料
		mtrl.diffuseAlbedo=float4(1,0,0,0);
		mtrl.specularAlbedo=float4(0.03,0.03,0.03,0);
	}

	if(greenBoxDist<minDist)
	{
		minDist=greenBoxDist;
		//光滑绿石
		mtrl.diffuseAlbedo=float4(0,0.5,0,0.1);
		mtrl.specularAlbedo=float4(0.11,0.11,0.11,0.23);
	}
	if(torus88Dist<minDist)
	{
		minDist=torus88Dist;
		//光滑黄塑料
		mtrl.diffuseAlbedo=float4(0.87f,0.87f,0.27f,0);
		mtrl.specularAlbedo=float4(0.02,0.02,0.02,0.7);
	}

	return minDist;
}





// If p is near a surface, the gradient will approximate the surface normal.
inline float3 getNormal(float3 p)
{
	float h = 0.0001f;

	return normalize(float3(
		distScene(p + float3(h, 0, 0)) - distScene(p - float3(h, 0, 0)),
		distScene(p + float3(0, h, 0)) - distScene(p - float3(0, h, 0)),
		distScene(p + float3(0, 0, h)) - distScene(p - float3(0, 0, h))));
}

//about floor
inline float rayFloorIntersection(float3 ro,float3 rd,float3 floorNormal,float3 floorPoint)
{
	return dot((floorPoint-ro),floorNormal)/dot(rd,floorNormal);
}

inline float2 mod2(float2 a,float2 b)
{
  //return (a/b-floor(a/b))*b;
  return a-floor(a/b)*b;
}
inline void getFloorTexture(float3 p,out PBRMtrl mtrl)
{
	float2 m = mod2(p.xz, float2(20,20)) - float2(10,10);
	mtrl.diffuseAlbedo= m.x * m.y > 0.0f ? float4(0.1f,0.1f,0.1f,0.3f) : float4(1.0f,1.0f,1.0f,0.3f);
	mtrl.specularAlbedo=float4(0.1,0.1,0.1,0.5);
}

bool rayMarchWithMtrl(float3 ro,float3 rd,out float3 surfacePoint,out float3 surfaceNormal,out PBRMtrl mtrl)
{
	//initialization
	surfacePoint=float3(0,0,0);
	surfaceNormal=float3(0,1,0);
	//初始红塑料
	mtrl.diffuseAlbedo=float4(1,0,0,0);
	mtrl.specularAlbedo=float4(0,0,0,0);

	//这一部分才是真正意义上的ray marching
	float t=0.0f;//distanse traveld by ray
	int i=0;//marchiing steps
	for(i=0;i<gMaxSteps+1;i++)
	{
		float dist=distSceneWithMtrl(ro+rd*t,mtrl);
		
		if(abs(dist) < gDistanceEpsilon * t * 2.0f || t >gNearFarDistance.y)
		{
			break;
		}
		t += dist;
	}

	//optimization,，就不让floor参与distance field了
	//用到off-line rendering中的方式，直接和floor plane求交
	float3 floorNormal=float3(0,1,0);
	float3 floorPoint=float3(0,-0.5,0);
	float distanceToFloor=rayFloorIntersection(ro,rd,floorNormal,floorPoint);

	if(distanceToFloor<t && distanceToFloor>=gNearFarDistance.x && distanceToFloor<=gNearFarDistance.y)//floor is closest
	{
		t=distanceToFloor;
		surfacePoint=ro+rd*distanceToFloor;
		surfaceNormal=floorNormal;
		getFloorTexture(surfacePoint,mtrl);

		return true;
	}
	else if(t>=gNearFarDistance.x && t<=gNearFarDistance.y)//rayMarching hit a surface
	{
		surfacePoint=ro+rd*t;
		surfaceNormal=getNormal(surfacePoint);

		return true;
	}

	return false;
}

inline float getShadow(float3 ro,float3 rd,float minDist,float maxDist,float k)//k控制软硬
{
	float res = 1.0;
    	for( float t=minDist; t < maxDist; )
   	{
        		float h =distScene(ro + rd*t);
        		if( h<gDistanceEpsilon )
            		return 0.0f;
        		res = min( res, k*h/t );//这里有个疑问，难道不应该是t越近整体越小才会越暗吗？
        		t += h;
    	}
    	return res;
}

inline float getAmbientOcclusion(float3 p,float3 n)
{
	float stepSize = 0.01f;
	float t = stepSize;
	float oc = 0.0f;
	for(int i = 0; i < 10; ++i)
	{
		float d = distScene(p + n * t);
		oc += t - d; // Actual distance to surface - distance field value
		t += stepSize;
	}

	return clamp(oc, 0, 1);
}

float3 calculateLightWithoutReflection(float3 p,float3 n,PBRMtrl mtrl,float3 rd)
{
	float3 toLight=normalize( -gLightVec.xyz);
	float lightDistance=gMaxLightDistance;
	float3 reflectedLight=normalize(reflect(-toLight,n));
	float3 diffuseCol=float3(0,0,0);
	float3 specularCol=float3(0,0,0);
	float3 ambientCol=gAmbientLight.rgb*mtrl.diffuseAlbedo.rgb;
	if(gLightVec.w>0.0f)//不是方向光
	{
		toLight=gLightVec.xyz-p;
		lightDistance=length(toLight);
		toLight/=lightDistance;
		reflectedLight=normalize(reflect(-toLight,n));
	}

	//soft shadow
	float shadowFactor=getShadow(p,toLight,10.0f*gDistanceEpsilon,lightDistance,16);
	if(shadowFactor>gDistanceEpsilon)//才有必要进行光照计算
	{
		//这里就用Lambert Phong Model吧,也没弄随距离衰减
		//待改为brdf
		float ndotL=max(0,dot(toLight,n));
		diffuseCol=mtrl.diffuseAlbedo.rgb*gLightCol.rgb*ndotL;
		//Fresnel Term
		float3 fresnelFactor=mtrl.specularAlbedo.rgb+(1-mtrl.specularAlbedo.rgb)*pow(1-saturate(dot(-rd,n)),5);
		specularCol=pow(saturate(dot(reflectedLight,-rd)),10*mtrl.specularAlbedo.a)*fresnelFactor*gLightCol.rgb*ndotL;
	}

	//AO
	float AOFactor=1.0f-getAmbientOcclusion(p,n);

	return shadowFactor*(diffuseCol+specularCol)+AOFactor*ambientCol;
}

//因为shader不支持递归，recursive ray tracing转为iterative实现
float3 computeColor(float3 ro,float3 rd)
{
	float3 totalCol=float3(0,0,0);
	float reflectionBlendFactor=1.0f;
	for(int depth=0;depth<=MAX_RAY_DEPTH;depth++)
	{
		float3 surfacePoint;
		float3 surfaceNormal;
		PBRMtrl mtrl;

		if(!rayMarchWithMtrl(ro,rd,surfacePoint,surfaceNormal,mtrl))//ray marchiing为空
		{
			//totalCol+=reflectionBlendFactor * texCUBE(SkyBoxS, rd).rgb;
			//特么通不过编译！loop中如果使用了tex系列且uv坐标随loop变动，就无法通过编译，这和mip map有关
			//——》解决办法：用lod后缀系列或grad后缀系列
			totalCol+=reflectionBlendFactor * texCUBElod(SkyBoxS, float4(rd,0)).rgb;
			break;
			
		}
	
		float3 directCol=calculateLightWithoutReflection(surfacePoint,surfaceNormal,mtrl,rd);
		totalCol+=reflectionBlendFactor * directCol;

		//for next
		if(mtrl.specularAlbedo.a<0.1f)//不够光滑
		{
			break;
		}
		//Fresnel Term
		float fresnelFactor=mtrl.specularAlbedo.rgb+(1-mtrl.specularAlbedo.rgb)*pow(1-saturate(dot(-rd,surfaceNormal)),5);
		reflectionBlendFactor*=fresnelFactor*mtrl.specularAlbedo.a;
		ro=surfacePoint;
		rd=normalize(reflect(rd,surfaceNormal));	
	}

	return totalCol;
}





void QuadVS(float3 posL :POSITION,
	float2 tex0 :TEXCOORD0,
	out float4 posH :POSITION,
	out float2 oTex0 :TEXCOORD0)
{
	posH=float4(posL,1.0f);
	oTex0=posL.xy;
}

float4 RayCastPS(float2 uv:TEXCOORD0) :COLOR
{
	float3 ro = gEyePosW;
	float3 rd = normalize(gCamForward * gNearFarDistance.x + gCamRight * uv.x * gNearClipHalfSize.x + gCamUp * uv.y*gNearClipHalfSize.y);
	float3 color = computeColor(ro, rd);

	//4xAA
	//float2 hps = 1.0f / (gResolution * 2.0);
	//float3 rd0 = normalize(gCamForward * gNearFarDistance.x + gCamRight * (uv.x - hps.x) * gNearClipHalfSize.x + gCamUp * uv.y*gNearClipHalfSize.y);
	//float3 rd1 = normalize(gCamForward * gNearFarDistance.x + gCamRight * (uv.x + hps.x) * gNearClipHalfSize.x + gCamUp * uv.y*gNearClipHalfSize.y);
	//float3 rd2 = normalize(gCamForward * gNearFarDistance.x + gCamRight * uv.x * gNearClipHalfSize.x + gCamUp * (uv.y - hps.y)*gNearClipHalfSize.y);
	//float3 rd3 = normalize(gCamForward * gNearFarDistance.x + gCamRight * uv.x * gNearClipHalfSize.x + gCamUp * (uv.y + hps.y)*gNearClipHalfSize.y);
	//float3 color = (computeColor(ro, rd0) + computeColor(ro, rd1) + computeColor(ro, rd2) + computeColor(ro, rd3)) / 4.0;

	return float4(color, 1.0f);

}

technique DistanceFieldTech
{
	pass p0
	{
		vertexShader = compile vs_3_0 QuadVS();
       		pixelShader  = compile ps_3_0 RayCastPS();

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}












