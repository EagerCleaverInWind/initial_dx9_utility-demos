#pragma  once

#include "D3DApp.h"


class DistanceField:public D3DApp
{
private:
	LPDIRECT3DVERTEXBUFFER9 mFullScreenQuad;
	IDirect3DVertexDeclaration9* mQuadDecl;
	IDirect3DCubeTexture9* mSkyBox;
	D3DXVECTOR4 mLightVec;
	D3DXCOLOR mLightCol;
	D3DXCOLOR mAmbientLight;

	//特定scene
	D3DXMATRIX mBlueSphereMat;
	D3DXMATRIX mRedSphereMat;
	D3DXMATRIX mGreenBoxMat;
	D3DXMATRIX mTorusMat;

	ID3DXEffect* mFX;
	//camera
	D3DXHANDLE mhEyePosW;
	D3DXHANDLE mhCamForward;
	D3DXHANDLE mhCamUp;
	D3DXHANDLE mhCamRight;
	//light
	D3DXHANDLE mhLightVec;
	D3DXHANDLE mhLightCol;
	D3DXHANDLE mhAmbientLight;
	D3DXHANDLE mhSkyBox;
	//特定scene
	D3DXHANDLE mhBlueSphereMat;
	D3DXHANDLE mhRedSphereMat;
	D3DXHANDLE mhGreenBoxMat;
	D3DXHANDLE mhTorusMat;


private:
	//self
	void CreateQuad()
	{
		e_pd3dDevice->CreateVertexDeclaration(VertexPTElements,&mQuadDecl);
		e_pd3dDevice->CreateVertexBuffer(4*sizeof(VertexPT),D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &mFullScreenQuad, 0);
		VertexPT* vpt=nullptr;
		mFullScreenQuad->Lock(0,0,(void**)&vpt,0);
		vpt[0].pos=D3DXVECTOR3(-1,1,0);
		vpt[0].tex0=D3DXVECTOR2(0,0);
		vpt[1].pos=D3DXVECTOR3(1,1,0);
		vpt[1].tex0=D3DXVECTOR2(1,0);
		vpt[2].pos=D3DXVECTOR3(1,-1,0);
		vpt[2].tex0=D3DXVECTOR2(1,1);
		vpt[3].pos=D3DXVECTOR3(-1,-1,0);
		vpt[3].tex0=D3DXVECTOR2(0,1);
		mFullScreenQuad->Unlock();
	}

	void BuildFX()
	{
		ID3DXBuffer *errors;
		HR(D3DXCreateEffectFromFile(e_pd3dDevice, "DistanceFieldPlusPlus.fx",
			0, 0, D3DXSHADER_DEBUG, 0, &mFX, &errors));
		if(errors)
			MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);

		//camera
		//mhEyePosW=mFX->GetParameterByName(0," gEyePosW");//CNMB，害我debug shader大半天
		//——》所以说需要养成检测返回值HRESULT的好习惯
		mhEyePosW=mFX->GetParameterByName(0,"gEyePosW");
		mhCamForward=mFX->GetParameterByName(0,"gCamForward");
		mhCamUp=mFX->GetParameterByName(0,"gCamUp");
		mhCamRight=mFX->GetParameterByName(0,"gCamRight");
		//light
		mhLightVec=mFX->GetParameterByName(0,"gLightVec");
		mhLightCol=mFX->GetParameterByName(0,"gLightCol");
		mhAmbientLight=mFX->GetParameterByName(0,"gAmbientLight");
		mhSkyBox=mFX->GetParameterByName(0,"gSkyBox");
		mFX->SetValue(mhLightVec,&mLightVec,sizeof(mLightVec));
		mFX->SetValue(mhLightCol,&mLightCol,sizeof(mLightCol));
		mFX->SetValue(mhAmbientLight,&mAmbientLight,sizeof(mAmbientLight));
		mFX->SetTexture(mhSkyBox,mSkyBox);
		//特定scene
		//mhBlueSphereMat=mFX->GetParameterByName(0," gBlueSphereMat");
		mhBlueSphereMat=mFX->GetParameterByName(0,"gBlueSphereMat");
		mhRedSphereMat=mFX->GetParameterByName(0,"gRedSphereMat");
		mhGreenBoxMat=mFX->GetParameterByName(0,"gGreenBoxMat");
		mhTorusMat=mFX->GetParameterByName(0,"gTorus88Mat");
		mFX->SetMatrix(mhBlueSphereMat,&mBlueSphereMat);
		mFX->SetMatrix(mhRedSphereMat,&mRedSphereMat);
		mFX->SetMatrix(mhGreenBoxMat,&mGreenBoxMat);
		mFX->SetMatrix(mhTorusMat,&mTorusMat);
		//在这设置了断点，发现shader中函数太多编译奇慢

		mFX->SetTechnique("DistanceFieldTech");
	}

	//太短，就不单独弄了吧
	//void CreateTexture();
	//void InitLight();
	//void InitSceneGeometry();


	//base
	LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd)
	{
		D3DApp::Object_Init(hInstance,hwnd);
		//create texture
		D3DXCreateCubeTextureFromFile(e_pd3dDevice,"grassenvmap1024.dds",&mSkyBox);
		CreateQuad();

		//initLight
		mLightVec=D3DXVECTOR4(1,-2,-1,0);
		mLightCol=D3DXCOLOR(1,1,1,1);
		mAmbientLight=D3DXCOLOR(0.15,0.15,0.15,1);

		//特定scene
		D3DXMatrixTranslation(&mBlueSphereMat,-3,3,0);
		D3DXMatrixInverse(&mBlueSphereMat,0,&mBlueSphereMat);
		D3DXMatrixTranslation(&mRedSphereMat,7,7,0);
		D3DXMatrixInverse(&mRedSphereMat,0,&mRedSphereMat);
		D3DXMatrixTranslation(&mGreenBoxMat,0,10,-30);
		D3DXMatrixInverse(&mGreenBoxMat,0,&mGreenBoxMat);
		D3DXMatrixTranslation(&mTorusMat,0,15,30);
		D3DXMatrixInverse(&mTorusMat,0,&mTorusMat);

		BuildFX();
		e_pCamera->SetPosition(D3DXVECTOR3(0,20,-15));
		//e_pCamera->SetTarget(D3DXVECTOR3(0,0,0));
		return S_OK;
	}

	void Update(float timeDelta)
	{
		D3DApp::Update(timeDelta);

	}

	void Draw()
	{
		D3DApp::Draw();

		//update gpu camera data
		D3DXVECTOR3 temp=e_pCamera->GetPosition();
		mFX->SetValue(mhEyePosW,&temp,sizeof(D3DXVECTOR3));
		temp=e_pCamera->GetLookDirection();
		mFX->SetValue(mhCamForward,&temp,sizeof(D3DXVECTOR3));
		temp=e_pCamera->GetUpVec();
		mFX->SetValue(mhCamUp,&temp,sizeof(D3DXVECTOR3));
		temp=e_pCamera->GetRightVec();
		mFX->SetValue(mhCamRight,&temp,sizeof(D3DXVECTOR3));

		UINT numPasses=0;
		mFX->Begin(&numPasses,0);
		mFX->BeginPass(0);
		//draw full screen quad
		e_pd3dDevice->SetVertexDeclaration(mQuadDecl);
		e_pd3dDevice->SetStreamSource(0,mFullScreenQuad,0,sizeof(VertexPT));//一开始复制过来时VertexPNT没改过来。。。
		e_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
		mFX->EndPass();
		mFX->End();

	}


public:
	DistanceField(HINSTANCE hInstance,int nShowCmd):D3DApp("heihei","real-time ray tracing demo",hInstance,nShowCmd)
	{

	}

	~DistanceField()
	{

	}

};