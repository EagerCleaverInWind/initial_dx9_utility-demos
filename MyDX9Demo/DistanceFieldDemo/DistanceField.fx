#include"distanceFieldUtility.fx"
//注释过，发现奇慢不是因为函数多，而是调用嵌套、次数太多

static const float2 gNearClipHalfSize=float2(4.0f/3.0f,1);//投影面在world space中尺寸，不是screen resolution
static const float2 gNearFarDistance=float2(1,5000);
static const float2 gResolution=float2(800,600);
static const float gDistanceEpsilon=0.0001f;
static const float gMaxSteps=30;

uniform extern float3 gEyePosW;
uniform extern float3 gCamForward;
uniform extern float3 gCamUp;
uniform extern float3 gCamRight;

//light source
static const float3 gSkyColor=float3(0.93,0.73,0.93);
//对于多个punctual light考虑用数组装？
uniform extern float4 gLightVec;//w component，点光源or方向光
uniform extern float4 gLightCol;//a component,距离衰减系数
uniform extern float4 gAmbientLight;
static const float gMaxLightDistance=2000.0f;
uniform extern texture gSkyBox;
sampler SkyBoxS = sampler_state
{
	Texture = <gSkyBox>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
          AddressV  = WRAP;
};

//特定scene，自己组合
uniform extern float4x4 gBlueSphereMat;
uniform extern float4x4 gRedSphereMat;
uniform extern float4x4 gGreenBoxMat;
uniform extern float4x4 gTorus88Mat;

//PS:特么还跟c一样，被调用函数要放在调用它的函数的前面

//特定scene，自己组合
//debug用
/*float sdSphere( float3 p, float s )
{
  return length(p)-s;
}*/
float distScene(float3 p)
{
	float4 posW=float4(p,1.0f);
	float blueSphereDist=sdSphere(mul(posW,gBlueSphereMat).xyz,3.0f);
	float redSphereDist=sdSphere(mul(posW,gRedSphereMat).xyz,7.0f);
	float greenBoxDist=udRoundBox(mul(posW,gGreenBoxMat).xyz,float3(7,8,9),1.8f);
	float torus88Dist=sdTorus88(mul(posW,gTorus88Mat).xyz,float2(3,5));

	return min(min(min(blueSphereDist,redSphereDist),greenBoxDist),torus88Dist);
}

//特定scene，自己组合
float4 getMtrl(float3 p,float threshold)
{
	float4 posW=float4(p,1.0f);
	float dist=sdSphere(mul(posW,gBlueSphereMat).xyz,3.0f);
	if(dist<threshold)
	{
		return float4(0,0,1,0.25f);
	}

	dist=sdSphere(mul(posW,gRedSphereMat).xyz,7.0f);
	if(dist<threshold)
	{
		return float4(1,0,0,0.25f);
	}

	dist=udRoundBox(mul(posW,gGreenBoxMat).xyz,float3(7,8,9),1.8f);
	if(dist<threshold)
	{
		return float4(0,1,0,0);
	}

	dist=sdTorus88(mul(posW,gTorus88Mat).xyz,float2(3,5));
	if(dist<threshold)
	{
		return float4(0.87f,0.87f,0.27f,0);
	}

	//error
	return float4(1,0,0,0);
}


void rayMarch(float3 ro,float3 rd,out int i,out float t)
{
	t=0.0f;
	i=0;
	for(i=0;i<gMaxSteps+1;i++)
	{
		float dist=distScene(ro+rd*t);
		//debug
		//float dist=length(ro+rd*t)-5.0f;

		// We make epsilon proportional to t so that we drop accuracy the further into the scene we get
		// We also drop the ray as soon as it leaves the clipping volume as defined by g_zFar
		//if(dist < gDistanceEpsilon * t * 2.0f || t >gNearFarDistance.y)
		if(abs(dist) < gDistanceEpsilon * t * 2.0f || t >gNearFarDistance.y)
		//if(abs(dist) < gDistanceEpsilon || t >gNearFarDistance.y)
		{
			break;
		}
		t += dist;
	}

	//debug用
	//t=100.0f;
	//if(i<=1)
	//t=100.0f;
}

// If p is near a surface, the gradient will approximate the surface normal.
float3 getNormal(float3 p)
{
	float h = 0.0001f;

	return normalize(float3(
		distScene(p + float3(h, 0, 0)) - distScene(p - float3(h, 0, 0)),
		distScene(p + float3(0, h, 0)) - distScene(p - float3(0, h, 0)),
		distScene(p + float3(0, 0, h)) - distScene(p - float3(0, 0, h))));
}

//about floor
float rayFloorIntersection(float3 ro,float3 rd,float3 floorNormal,float3 floorPoint)
{
	return dot((floorPoint-ro),floorNormal)/dot(rd,floorNormal);
}

float2 mod2(float2 a,float2 b)
{
  //return (a/b-floor(a/b))*b;
  return a-floor(a/b)*b;
}
float4 getFloorTexture(float3 p)
{
	float2 m = mod2(p.xz, float2(10,10)) - float2(5,5);
	return m.x * m.y > 0.0f ? float4(0.1f,0.1f,0.1f,0.1f) : float4(1.0f,1.0f,1.0f,1.0f);
}





float getShadow(float3 ro,float3 rd,float minDist,float maxDist,float k)//k控制软硬
{
	float res = 1.0;
    	for( float t=minDist; t < maxDist; )
   	{
        		float h =distScene(ro + rd*t);
        		if( h<gDistanceEpsilon )
            		return 0.0f;
        		res = min( res, k*h/t );//这里有个疑问，难道不应该是t越近整体越小才会越暗吗？
        		t += h;
    	}
    	return res;
}

float getAmbientOcclusion(float3 p,float3 n)
{
	float stepSize = 0.01f;
	float t = stepSize;
	float oc = 0.0f;
	for(int i = 0; i < 10; ++i)
	{
		float d = distScene(p + n * t);
		oc += t - d; // Actual distance to surface - distance field value
		t += stepSize;
	}

	return clamp(oc, 0, 1);
}

//待搞个Mtrl struct，别再委屈金属
float3 calculateLight(float3 p,float3 n,float4 mtrl)
{
	float3 toLight=normalize( -gLightVec.xyz);
	float lightDistance=gMaxLightDistance;
	float3 toEye=gEyePosW-p;
	float3 reflectEye=normalize(reflect(-toEye,n));
	float3 diffuseCol=float3(0,0,0);
	float3 specularCol=float3(0,0,0);
	float3 ambientCol=gAmbientLight.rgb*mtrl.rgb;
	float3 reflectionCol=float3(0,0,0);
	if(gLightVec.w>0.0f)//不是方向光
	{
		toLight=gLightVec.xyz-p;
		lightDistance=length(toLight);
		toLight/=lightDistance;

	}

	//soft shadow
	float shadowFactor=getShadow(p,toLight,10.0f*gDistanceEpsilon,lightDistance,16);
	if(shadowFactor>gDistanceEpsilon)//才有必要进行光照计算
	{
		//这里就用Lambert Phong Model吧,也没弄随距离衰减
		float ndotL=max(0,dot(toLight,n));
		diffuseCol=mtrl.rgb*gLightCol.rgb*ndotL;
		specularCol=pow(saturate(dot(toLight,reflectEye)),10*mtrl.a)*gLightCol.rgb*ndotL;
	}

	//AO
	float AOFactor=1.0f-getAmbientOcclusion(p,n);

	//reflection
	if(mtrl.a>gDistanceEpsilon*10)
	{
		//Fresnel项，因为没有specular albedo只好委屈金属，就只和入射角有关了
		float fresnelFactor=0.1+0.9*pow(1-saturate(dot(toEye,n)),5);
		//debug用
		//float fresnelFactor=1.0f;

		//哈哈，real time试试递归
		//reflectionCol=computeColor(p,reflectEye);

		//optimization,直接用sky box的
		reflectionCol=texCUBE(SkyBoxS, reflectEye).rgb;
		//待改为computeColor传recursion limited times

		reflectionCol*=fresnelFactor;//这其实就是在做逆向specular brdf的F项
	}

	//total
	return shadowFactor*(diffuseCol+specularCol)+AOFactor*ambientCol+mtrl.a*reflectionCol;
	//对于完全正规的PBR，两个都属于dw光源所以直接叠加，
	//这里的光滑度修饰因子其实是不存在的（应该放到对reflection的brdf中作用也就是粗糙度参数），
	//这里光滑度是用来当阀值看做不做reflection的，也算是粗略做逆向specular brdf的D项和G项
}

float3 computeColor(float3 ro,float3 rd)
{
	float3 surfacePoint;
	float3 surfaceNormal;
	float t;//distanse traveld by ray
	float4 mtrl=float4(1,1,1,0.0f);//约定前三个component表texture color（diffuse albedo），
	//后一个表示光滑度决定reflection权重，以及specular项中的粗糙度
	//specular albedo以及金属性就算了，重点又不是PBR

	int i;//marching steps
	rayMarch(ro,rd,i,t);

	//optimization,，就不让floor参与distance field了
	//用到off-line rendering中的方式，直接和floor plane求交
	float3 floorNormal=float3(0,1,0);
	float3 floorPoint=float3(0,-0.5,0);
	float distanceToFloor=rayFloorIntersection(ro,rd,floorNormal,floorPoint);

	//debug用
	//distanceToFloor=19;
	//t=100;

	if(distanceToFloor<t && distanceToFloor>=gNearFarDistance.x && distanceToFloor<=gNearFarDistance.y)//floor is closest
	//debug
	//if(t<0)
	{
		t=distanceToFloor;
		surfacePoint=ro+rd*distanceToFloor;
		surfaceNormal=floorNormal;
		mtrl=getFloorTexture(surfacePoint);
	}
	else if(t>=gNearFarDistance.x && t<=gNearFarDistance.y)//rayMarching hit a surface
	{
		surfacePoint=ro+rd*t;
		surfaceNormal=getNormal(surfacePoint);
		mtrl=getMtrl(surfacePoint,2*t*gDistanceEpsilon);
		//mtrl=getMtrl(surfacePoint,10*gDistanceEpsilon);
		//mtrl=getMtrl(surfacePoint,gDistanceEpsilon);
	}
	else//sky color
	{
		//return gSkyColor;
		//其实可以用ray direction sample sky box啊!
		return texCUBE(SkyBoxS, rd).rgb;
	}

	float3 oColor=calculateLight(surfacePoint,surfaceNormal,mtrl);//soft shadow & ambient occlusion & reflection都在里面
	//可以在这里加fog & depth of field等
	return oColor;
}





void QuadVS(float3 posL :POSITION,
	float2 tex0 :TEXCOORD0,
	out float4 posH :POSITION,
	out float2 oTex0 :TEXCOORD0)
{
	posH=float4(posL,1.0f);
	oTex0=posL.xy;
}

float4 RayCastPS(float2 uv:TEXCOORD0) :COLOR
{
	float3 ro = gEyePosW;
	float3 rd = normalize(gCamForward * gNearFarDistance.x + gCamRight * uv.x * gNearClipHalfSize.x + gCamUp * uv.y*gNearClipHalfSize.y);
	//debug用
	//float3 ro = float3(0,20,-15);
	//float3 rd = normalize(float3(0,-1,0) * gNearFarDistance.x +float3(1,0,0) * uv.x * gNearClipHalfSize.x + float3(0,0,1) * uv.y*gNearClipHalfSize.y);

	float3 color = computeColor(ro, rd);

	//4xAA
	//float2 hps = 1.0f / (gResolution * 2.0);
	//float3 rd0 = normalize(gCamForward * gNearFarDistance.x + gCamRight * (uv.x - hps.x) * gNearClipHalfSize.x + gCamUp * uv.y*gNearClipHalfSize.y);
	//float3 rd1 = normalize(gCamForward * gNearFarDistance.x + gCamRight * (uv.x + hps.x) * gNearClipHalfSize.x + gCamUp * uv.y*gNearClipHalfSize.y);
	//float3 rd2 = normalize(gCamForward * gNearFarDistance.x + gCamRight * uv.x * gNearClipHalfSize.x + gCamUp * (uv.y - hps.y)*gNearClipHalfSize.y);
	//float3 rd3 = normalize(gCamForward * gNearFarDistance.x + gCamRight * uv.x * gNearClipHalfSize.x + gCamUp * (uv.y + hps.y)*gNearClipHalfSize.y);
	//float3 color = (computeColor(ro, rd0) + computeColor(ro, rd1) + computeColor(ro, rd2) + computeColor(ro, rd3)) / 4.0;

	return float4(color, 1.0f);

}

technique DistanceFieldTech
{
	pass p0
	{
		vertexShader = compile vs_3_0 QuadVS();
       		pixelShader  = compile ps_3_0 RayCastPS();

	      	ZEnable=FALSE;
        		ZWriteEnable=FALSE;
	}
}












